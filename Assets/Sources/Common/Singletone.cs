using UnityEngine;

namespace Purse.Common
{
	[DisallowMultipleComponent]
	public abstract class Singletone<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static T instance;
		private static GameObject singletonesContainer;

		protected virtual void Awake()
		{
			if (instance != null)
			{
				Destroy(this);
				if (Debug.isDebugBuild)
					Debug.LogWarning("Attempt to create a second instance of singletone!", this);
			}
			else
				instance = this as T;

			singletonesContainer = gameObject;
		}

		public static T Instance
		{
			get
			{
				if (!instance)
				{
					if (Debug.isDebugBuild && !singletonesContainer)
						singletonesContainer = new GameObject("[SINGLETONES CONTAINER]");
					singletonesContainer.AddComponent<T>();
				}

				return instance;
			}
		}

		protected virtual void OnDestroy()
		{
			instance = null;
		}
	}
}