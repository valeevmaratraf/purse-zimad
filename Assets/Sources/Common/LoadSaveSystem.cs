using UnityEngine;
using UnityEngine.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

namespace Purse.Common
{
	public enum SaveLoadType { Json, Binary, PlayerPrefs }
	public class LoadSaveSystem : Singletone<LoadSaveSystem>
	{

		public void SaveData(SaveLoadType type, object data, string fileName)
		{
			switch (type)
			{
				case SaveLoadType.Binary:
					FileStream fs = new FileStream(Application.persistentDataPath + string.Format("/{0} binary.dat", fileName), FileMode.OpenOrCreate);
					BinaryFormatter bf = new BinaryFormatter();
					bf.Serialize(fs, data);
					fs.Close();
					break;

				case SaveLoadType.Json:
					string serializedData = JsonUtility.ToJson(data);
					File.WriteAllText(Application.persistentDataPath + string.Format("/{0} json.txt", fileName), serializedData);
					break;

				case SaveLoadType.PlayerPrefs:
					PlayerPrefs.SetString(fileName, JsonUtility.ToJson(data));
					PlayerPrefs.Save();
					break;
			}
		}

		public T LoadData<T>(SaveLoadType type, string fileName)
		{
			string path;
			object loadedObj;

			switch (type)
			{
				case SaveLoadType.Json:
					path = Application.persistentDataPath + string.Format("/{0} json.txt", fileName);
					if (File.Exists(path))
					{
						string data = File.ReadAllText(Application.persistentDataPath + string.Format("/{0} json.txt", fileName));
						return JsonUtility.FromJson<T>(data);
					}
					throw new Exception("Saved data is not exist!");

				case SaveLoadType.Binary:
					path = Application.persistentDataPath + string.Format("/{0} binary.dat", fileName);
					if (File.Exists(path))
					{
						FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
						BinaryFormatter bf = new BinaryFormatter();
						loadedObj = bf.Deserialize(fs);
						fs.Close();
						return (T)loadedObj;
					}
					throw new Exception("Saved data is not exist!");

				case SaveLoadType.PlayerPrefs:
					string s = PlayerPrefs.GetString(fileName);
					return JsonUtility.FromJson<T>(PlayerPrefs.GetString(fileName));

				default:
					throw new Exception("Invalid save/load code!");
			}
		}
	}
}