using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Purse.Common
{
	public static class Utilites
	{
		public static string HandleText(string text, params string[] args)
		{
			for (int i = 0; i < args.Length; i++)
				text = text.Replace(string.Format("[value {0}]", i), args[i]);
			return text;
		}
	}
}