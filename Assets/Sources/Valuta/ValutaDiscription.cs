using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Purse
{
	[CreateAssetMenu(fileName = "New valuta", menuName = "Application/New valuta")]
	public class ValutaDiscription : ScriptableObject
	{
		[SerializeField] private string valutaName;
		[SerializeField] private int id;

		public int ID { get => id; }
		public string ValutaName { get => valutaName; }
	}
}