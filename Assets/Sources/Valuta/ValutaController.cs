using Purse.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Purse
{
	public class ValutaController : MonoBehaviour
	{
		[SerializeField] private ValutaDiscription valutaDiscr;
		[SerializeField] private TextMeshProUGUI text;
		[SerializeField] private TextMeshProUGUI addButtonText;

		private int count;
		private string initialText;

		public ValutaDiscription ValutaDiscription { get => valutaDiscr; }

		private void Start()
		{
			initialText = text.text;
			text.SetText(Utilites.HandleText(text.text, valutaDiscr.name + "s", Count.ToString()));
			addButtonText.SetText(Utilites.HandleText(addButtonText.text, valutaDiscr.name.ToLower()));
		}

		public int Count
		{
			get { return count; }
			set
			{
				if (Debug.isDebugBuild && count < 0)
					Debug.LogWarning("Trying set less than 0 valuta count!", this);
				count = value;
			}
		}

		public void AddValuta()
		{
			Count++;
			RefreshRepresentation();
		}

		public void SaveValuta(int typeCode)
		{
			ValutaData data = new ValutaData(ValutaDiscription, Count);
			LoadSaveSystem.Instance.SaveData((SaveLoadType)typeCode, data, valutaDiscr.name);
		}

		public void LoadValuta(int typeCode)
		{
			ValutaData data = LoadSaveSystem.Instance.LoadData<ValutaData>((SaveLoadType)typeCode, valutaDiscr.name);
			Count = data.Count;
			RefreshRepresentation();
		}

		public void RefreshRepresentation()
		{
			text.text = initialText;
			text.SetText(Utilites.HandleText(text.text, valutaDiscr.name + "s", Count.ToString()));
		}

		public void ResetCount()
		{
			Count = 0;
			RefreshRepresentation();
		}
	}

	[Serializable]
	public class ValutaData
	{
		public int ID;
		public string Name;
		public int Count;

		public ValutaData(ValutaDiscription v, int count)
		{
			ID = v.ID;
			Count = count;
			Name = v.ValutaName;
		}
	}
}